package appModule;

import org.openqa.selenium.WebDriver;
import pageObjects.Home_Page;
import pageObjects.Login_Page;
import utilities.ExcelUtils;
import utilities.Log;

public class SignIn_Action
{
	/*
	public static void Execute(WebDriver driver, String sUsername, String sPassword)
	{
		// Pass Arguments (Username and Password) as string
		
		Home_Page.lnk_MyAccount(driver).click();
		
		Login_Page.txtbx_UserName(driver).sendKeys(sUsername);
		Login_Page.txtbx_Password(driver).sendKeys(sPassword);
		Login_Page.btn_Login(driver).click();
	*/
	public static void Execute(WebDriver driver) throws Exception
	{
		String sUserName = ExcelUtils.getCellData(1, 1);
		Log.info("Username picked from Excel is "+ sUserName );
		
		String sPassword = ExcelUtils.getCellData(1, 2);
		Log.info("Password picked from Excel is "+ sPassword );
		
		Home_Page.lnk_MyAccount(driver).click();
		Log.info("Click action performed on My Account link");
		
		Login_Page.txtbx_UserName(driver).sendKeys(sUserName);
		Log.info("Username entered in the Username text box");
		
		Login_Page.txtbx_Password(driver).sendKeys(sPassword);
		Log.info("Password entered in the Password text box");
		
		Login_Page.btn_Login(driver).click();
		Log.info("Click action performed on Submit button");
	}
}
