package automationFramework;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import pageObjects.Home_Page;
import org.openqa.selenium.firefox.FirefoxDriver;
import pageObjects.Home_Page;
import appModule.SignIn_Action;
import utilities.Constant;

public class Global_Var_TC
{
	private static WebDriver driver = null;

	public static void main(String[] args) throws Exception
	{
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		// Launch the Online Store Website using Constant Variable
		driver.get(Constant.URL);
		
		// Pass Constant Variables as arguments to Execute method
		SignIn_Action.Execute(driver);
		
		System.out.println("Login successfully with parameters");
		
		Home_Page.lnk_LogOut(driver).click();
		
		driver.quit();
	}

}
